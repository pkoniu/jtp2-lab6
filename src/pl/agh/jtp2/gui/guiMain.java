package pl.agh.jtp2.gui;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by pkoniu on 4/26/2014.
 */
public class guiMain {
    private JFrame frame;
    private JLabel label;
    private int x;
    private int y;

    public void go() {
        frame = new JFrame();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        //JButton LabelButton = new JButton("Change label.");
        //LabelButton.addActionListener(new LabelListener());

        //JButton CircleButton = new JButton("Change circle color.");
        //CircleButton.addActionListener(new CircleListener());

        //label = new JLabel("I'm a text!");
        //guiDraw circle = new guiDraw();

        SimpleAnimation circleanim = new SimpleAnimation();

        frame.getContentPane().add(circleanim);
        //frame.getContentPane().add(BorderLayout.SOUTH, CircleButton);
        //frame.getContentPane().add(BorderLayout.CENTER, circle);
        //frame.getContentPane().add(BorderLayout.WEST, label);
        //frame.getContentPane().add(BorderLayout.EAST, LabelButton);
        frame.setSize(450,450);
        frame.setVisible(true);

        for(int i = 0; i < 150; i++) {
            x += 2;
            y += 2;
            circleanim.repaint();
            try {
                Thread.sleep(25);
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }
    }

    class CircleListener implements ActionListener {
        public void actionPerformed(ActionEvent event) {
            frame.repaint();
        }
    }

    class LabelListener implements ActionListener {
        public void actionPerformed(ActionEvent event) {
            label.setText("Good job!");
        }
    }

    class SimpleAnimation extends JPanel {
        public void paintComponent(Graphics g) {
            g.setColor(Color.black);
            g.fillRect(0, 0, this.getWidth(), this.getHeight());

            g.setColor(Color.white);
            g.fillOval(x, y, 100, 100);
        }
    }
}
