package pl.agh.jtp2.thread.animation;

import javax.swing.*;
import java.awt.*;

/**
 * Created by pkoniu on 4/27/2014.
 */
public class CircleAnimation {
    private JFrame frame;
    private JLabel label;
    private int x;
    private int y;

    public void moveCircleAroundTheScreen() {
        frame = new JFrame();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        SimpleAnimation AnimatedCircle = new SimpleAnimation();

        frame.getContentPane().add(AnimatedCircle);
        frame.setSize(450,450);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);

        for(int i = 0; i < 150; i++) {
            x += 2;
            y += 2;
            AnimatedCircle.repaint();
            try {
                Thread.sleep(25);
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }
    }

    class SimpleAnimation extends JPanel {
        public void paintComponent(Graphics g) {
            g.setColor(Color.black);
            g.fillRect(0, 0, this.getWidth(), this.getHeight());

            g.setColor(Color.white);
            g.fillOval(x, y, 100, 100);
        }
    }
}
