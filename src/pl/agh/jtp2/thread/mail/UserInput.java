package pl.agh.jtp2.thread.mail;

import org.apache.commons.mail.EmailException;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by pkoniu on 4/27/2014.
 */
public class UserInput extends JPanel implements ActionListener {
    private JTextField textField;

    public UserInput() {
        super(new GridBagLayout());

        textField = new JTextField(20);
        textField.addActionListener(this);

        GridBagConstraints c = new GridBagConstraints();
        c.gridwidth = GridBagConstraints.REMAINDER;

        c.fill = GridBagConstraints.HORIZONTAL;
        add(textField, c);
    }

    public void actionPerformed(ActionEvent evt) {
        String TextProvided = textField.getText();
        SendEmail email = new SendEmail();
        try {
            email.makeMsgAndSend(TextProvided);
        } catch (EmailException e) {
            e.printStackTrace();
        }
        System.exit(0);
    }

    /**
     * Create the GUI and show it.  For thread safety,
     * this method should be invoked from the
     * event dispatch thread.
     */
    private static void createAndShowGUI() {
        //Create and set up the window.
        JFrame frame = new JFrame("TextDemo");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        //Add contents to the window.
        frame.add(new UserInput());

        //Display the window.
        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
    }

    public static void getUsersEmail() {
        //Schedule a job for the event dispatch thread:
        //creating and showing this application's GUI.
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                createAndShowGUI();
            }
        });
    }
}
