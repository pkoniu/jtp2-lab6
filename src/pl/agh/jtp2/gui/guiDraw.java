package pl.agh.jtp2.gui;

import javax.swing.*;
import java.awt.*;

/**
 * Created by pkoniu on 4/26/2014.
 */
public class guiDraw extends JPanel{
    public void paintComponent(Graphics g) {
        Graphics2D circle = (Graphics2D) g;

        int red = (int) (Math.random() * 256);
        int green = (int) (Math.random() * 256);
        int blue = (int) (Math.random() * 256);
        Color start = new Color(red, green, blue);

        red = (int) (Math.random() * 256);
        green = (int) (Math.random() * 256);
        blue = (int) (Math.random() * 256);
        Color end = new Color(red, green, blue);

        GradientPaint gradient = new GradientPaint(70, 70, start, 150, 150, end);
        circle.setPaint(gradient);
        circle.fillOval(100, 100, 100, 100);
    }
}
