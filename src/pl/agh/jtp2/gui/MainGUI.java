package pl.agh.jtp2.gui;

import pl.agh.jtp2.thread.date.ThrowDateOnDisplay;
import pl.agh.jtp2.thread.folder.FolderObserver;
import pl.agh.jtp2.thread.mail.UserInput;
import pl.agh.jtp2.thread.music.VideoMusicMaker;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

/**
 * Created by pkoniu on 4/26/2014.
 */
public class MainGUI implements Runnable{
    public void run() {
        JFrame frame = new JFrame();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        JPanel ButtonPanel = new JPanel();
        ButtonPanel.setBackground(Color.darkGray);
        ButtonPanel.setLayout(new BoxLayout(ButtonPanel, BoxLayout.Y_AXIS));

        JPanel LabelPanel = new JPanel();
        LabelPanel.setBackground(Color.orange);
        LabelPanel.setLayout(new BoxLayout(LabelPanel, BoxLayout.Y_AXIS));

        JLabel mainLabel = new JLabel("Start and stop multiple threads simple by clicking button beneath.");
        JLabel gap = new JLabel(" ");
        JLabel gap1 = new JLabel(" ");
        JLabel gap2 = new JLabel(" ");
        JLabel musicVideoLabel = new JLabel("Create simple melody and visualize it.");
        JLabel folderObserverLabel = new JLabel("Start observing a folder in a current directory (it'll be created).");
        JLabel emailLabel = new JLabel("Send a simple email to a address provided in next window.");
        JLabel dateLabel = new JLabel("Display current date & time.");

        JButton StartMusicButton = new JButton("Start Music");
        StartMusicButton.addActionListener(new MusicListener());
        //JButton StopMusicButton = new JButton("Stop");

        JButton StartObserveButton = new JButton("Start observing");
        StartObserveButton.addActionListener(new FolderListener());
        //JButton StopObserveButton = new JButton("Stop");

        JButton StartEmailButton = new JButton("Start email");
        StartEmailButton.addActionListener(new EmailListener());
        //JButton StopEmailButton = new JButton("Stop");

        JButton StartDateButton = new JButton("Start date");
        StartDateButton.addActionListener(new DateListener());
        //JButton StopDateButton = new JButton("Stop");

        JButton ExitButton = new JButton("Close program");
        ExitButton.addActionListener(new ExitListener());

        LabelPanel.add(BorderLayout.WEST, musicVideoLabel);
        LabelPanel.add(BorderLayout.WEST, gap);
        LabelPanel.add(BorderLayout.WEST, folderObserverLabel);
        LabelPanel.add(BorderLayout.WEST, gap1);
        LabelPanel.add(BorderLayout.WEST, emailLabel);
        LabelPanel.add(BorderLayout.WEST, gap2);
        LabelPanel.add(BorderLayout.WEST, dateLabel);

        ButtonPanel.add(BorderLayout.EAST, StartMusicButton);
        ButtonPanel.add(BorderLayout.EAST, StartObserveButton);
        ButtonPanel.add(BorderLayout.EAST, StartEmailButton);
        ButtonPanel.add(BorderLayout.EAST, StartDateButton);

        frame.getContentPane().add(BorderLayout.NORTH, mainLabel);
        frame.getContentPane().add(BorderLayout.WEST, ButtonPanel);
        frame.getContentPane().add(BorderLayout.EAST, LabelPanel);
        frame.getContentPane().add(BorderLayout.SOUTH, ExitButton);

        frame.setSize(500, 250);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
    }

    class MusicListener implements ActionListener {
        public void actionPerformed(ActionEvent event) {
            VideoMusicMaker video = new VideoMusicMaker();
            video.makeVideo();
        }
    }

    class FolderListener implements ActionListener {
        public void actionPerformed(ActionEvent event) {
            FolderObserver folder = new FolderObserver();
            try {
                folder.observe();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    class EmailListener implements ActionListener {
        public void actionPerformed(ActionEvent event) {
            UserInput input = new UserInput();
            input.getUsersEmail();
        }
    }

    class DateListener implements ActionListener {
        public void actionPerformed(ActionEvent event) {
            ThrowDateOnDisplay date = new ThrowDateOnDisplay();
            date.displayDate();
        }
    }

    class ExitListener implements ActionListener {
        public void actionPerformed(ActionEvent event) {
            System.exit(0);
        }
    }
}
