package pl.agh.jtp2;

import pl.agh.jtp2.gui.guiMain;

/**
 * Created by pkoniu on 4/26/2014.
 */
public class Main {
    public static void main(String[] args) {
        guiMain MyGui = new guiMain();
        MyGui.go();
    }
}
