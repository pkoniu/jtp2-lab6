package pl.agh.jtp2.thread.folder;

import java.io.File;
import java.io.IOException;
import java.nio.file.*;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;

import static java.nio.file.StandardWatchEventKinds.*;

/**
 * Created by pkoniu on 4/27/2014.
 */
public class FolderObserver {
    private Path PathDirectory;
    private static final Logger logger = Logger.getLogger(FolderObserver.class.getName());

    public FolderObserver() {
        File FolderToObserve = new File("FolderToObserve");
        if(!FolderToObserve.exists()) {
            FolderToObserve.mkdir(); //todo : log
        }
        Path path = FolderToObserve.toPath();
        this.setPathDirectory(path);
    }

    public void setPathDirectory(Path path) {
        this.PathDirectory = path;
    }

    public void observe() throws IOException, InterruptedException {
        logger.entering(getClass().getName(), "observe");
        logger.log(Level.INFO, "Starting monitoring the given directory.");

        FileHandler handler = new FileHandler("lab4-folderobserver-log.xml", true); //todo : improve log
        logger.addHandler(handler);

        String type;

        FileSystem UserFS = this.PathDirectory.getFileSystem();

        try {
            WatchService FolderMonitor = UserFS.newWatchService();
            this.PathDirectory.register(FolderMonitor, ENTRY_CREATE, ENTRY_DELETE);

            WatchKey key;


            while (true) {
                key = FolderMonitor.take();

                WatchEvent.Kind<?> tmp;

                for (WatchEvent<?> EventsInFolder : key.pollEvents()) {
                    tmp = EventsInFolder.kind();
                    Path RecentlyCreatedPath = ((WatchEvent<Path>) EventsInFolder).context();

                    if (tmp == ENTRY_CREATE) {
                        logger.log(Level.INFO, "Created new file in the directory: " + RecentlyCreatedPath.toString());
                        type = this.giveMeType(String.valueOf(RecentlyCreatedPath));
                        this.actWithFile(type, RecentlyCreatedPath);
                    } else if (tmp == ENTRY_DELETE) {
                        logger.log(Level.INFO, "Deleted/moved a file or directory: " + RecentlyCreatedPath);
                    } else if (tmp == OVERFLOW) {
                        logger.log(Level.INFO, "An event has been lost.");
                        continue;
                    }
                }
                if (!key.reset()) {
                    break;
                }
            }
        } catch (IOException ioe) {
            logger.log(Level.SEVERE, "IOException while watching the directory!\n" + ioe.getMessage());
        } catch (InterruptedException ie) {
            logger.log(Level.SEVERE, "InterruptedException while watching the directory!\n" + ie.getMessage());
        } catch (IllegalArgumentException iae) {
            logger.log(Level.SEVERE, "IllegalArgumentException while watching the directory!" + iae.getMessage());
        }
        logger.exiting(getClass().getName(), "observe");
    }

    private String giveMeType(String newFileName) {
        String type = "";
        int i = newFileName.lastIndexOf(".");
        if(i > 0) {
            type = newFileName.substring(i+1);
        }
        if(type.equals("")) {
            type = "folder";
        }
        return type;
    }

    private void actWithFile (String type, Path pathToFile) throws IOException {
        FilesSorter fs = new FilesSorter(type);
        fs.sortIt(this.PathDirectory, pathToFile);
    }
}

//TODO: LOgger to FILE!!!!!
