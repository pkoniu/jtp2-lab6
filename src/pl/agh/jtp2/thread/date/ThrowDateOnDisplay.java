package pl.agh.jtp2.thread.date;

import javax.swing.*;
import java.awt.*;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by pkoniu on 4/27/2014.
 */
public class ThrowDateOnDisplay {
    public static void displayDate() {
        JFrame frame = new JFrame();
        frame.setTitle("Current date");
        //frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setPreferredSize(new Dimension(250, 80));

        String date = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date());
        JLabel l = new JLabel(date);

        frame.getContentPane().add(l);

        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);

    }
}
