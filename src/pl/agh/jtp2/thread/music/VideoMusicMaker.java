package pl.agh.jtp2.thread.music;

import javax.sound.midi.*;
import javax.swing.*;
import java.awt.*;

/**
 * Created by pkoniu on 4/27/2014.
 */
public class VideoMusicMaker {
    static JFrame f = new JFrame("Music video!");
    static MyDrawPanel ml;

    public static void makeVideo() {
        VideoMusicMaker player = new VideoMusicMaker();
        player.startPlaying();
    }

    public void setUpGUI() {
        ml = new MyDrawPanel();
        f.setContentPane(ml);
        f.setBounds(40,40,200,200);
        f.setLocationRelativeTo(null);
        f.setVisible(true);
    }

    public void startPlaying() {
        setUpGUI();

        try {
            Sequencer sequence = MidiSystem.getSequencer();
            sequence.open();

            sequence.addControllerEventListener(ml, new int[] {127});

            Sequence seq = new Sequence(Sequence.PPQ, 4);
            Track track = seq.createTrack();


            int r;
            for (int i = 5; i < 60; i += 4) {
                r = (int) ((Math.random() * 50) + 1);
                track.add(makeEvent(144, 1, r, 100, i));
                track.add(makeEvent(176, 1, 127, 0, i));
                track.add(makeEvent(128, 1, r, 100, i + 2));
            }

            sequence.setSequence(seq);
            sequence.start();
            sequence.setTempoInBPM(150);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public static MidiEvent makeEvent(int comd, int chan, int one, int two, int tick) {
        MidiEvent event = null;
        try {
            ShortMessage a = new ShortMessage();
            a.setMessage(comd, chan, one, two);
            event = new MidiEvent(a, tick);
        } catch (Exception e) {
            e.getMessage();
        }
        return event;
    }

    class MyDrawPanel extends JPanel implements ControllerEventListener {
        boolean msg = false;

        public void controlChange(ShortMessage event) {
            msg = true;
            repaint();
        }

        public void paintComponent(Graphics g) {
            if(msg) {
                int r = (int) (Math.random() * 250);
                int gr = (int) (Math.random() * 250);
                int b = (int) (Math.random() * 250);

                g.setColor(new Color(r,gr,b));

                int ht = (int) ((Math.random() * 120) + 10);
                int wth = (int) ((Math.random() * 120) + 10);

                int x = (int) ((Math.random() * 40) + 10);
                int y = (int) ((Math.random() * 40) + 10);

                g.fillRect(x, y, ht, wth);
                msg = false;
            }
        }
    }
}
