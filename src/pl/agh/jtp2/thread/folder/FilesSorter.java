package pl.agh.jtp2.thread.folder;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.logging.Level;
import java.util.logging.Logger;

import static java.nio.file.StandardCopyOption.ATOMIC_MOVE;
import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;

/**
 * Created by pkoniu on 4/27/2014.
 */
public class FilesSorter {
    private String extension;
    private static final Logger logger = Logger.getLogger(FilesSorter.class.getName());

    public FilesSorter(String ext) {
        this.setExtension(ext);
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    public void sortIt(Path path, Path FileName) throws IOException {
        logger.entering(getClass().getName(), "sortIt");
        logger.log(Level.INFO, "Sorting new file into directory (if it's been a folder, program does nothing).");
        String StringPath = path.toString();

        if (this.extension.equals("folder")) {
            return;
        } else if (this.extension.equals("txt") || this.extension.equals("doc") || this.extension.equals("docx")) {
            File folderDocs = new File(StringPath + ("\\Documents"));

            if (!folderDocs.exists()) {
                folderDocs.mkdir();
                logger.log(Level.INFO, "Created folder named \"Documents\"");
            }

            Path sourceDocs = path.resolve(FileName);
            Path targetDocs = folderDocs.toPath().resolve(FileName);

            try {
                Files.move(sourceDocs, targetDocs, ATOMIC_MOVE);
            } catch (IOException e) {
                logger.log(Level.SEVERE, "IOException while dealing with documents moved to the directory.\n" + e.getMessage());
            }

        } else if (this.extension.equals("wma") || this.extension.equals("mp3")) {
            File folderMsc = new File(StringPath + ("\\Music"));

            if (!folderMsc.exists()) {
                folderMsc.mkdir();
                logger.log(Level.INFO, "Created folder named \"Music\"");
            }

            Path sourceMsc = path.resolve(FileName);
            Path targetMsc = folderMsc.toPath().resolve(FileName);

            try {
                Files.move(sourceMsc, targetMsc, REPLACE_EXISTING);
            } catch (IOException e) {
                logger.log(Level.SEVERE, "IOException while dealing with music files moved to the directory.\n" + e.getMessage());
            }

        } else if (this.extension.equals("jpg") || this.extension.equals("bmp")) {
            File folderPct = new File(StringPath + ("\\Pictures"));

            if (!folderPct.exists()) {
                folderPct.mkdir();
                logger.log(Level.INFO, "Created folder named \"Pictures\"");
            }

            Path sourcePct = path.resolve(FileName);
            Path targetPct = folderPct.toPath().resolve(FileName);

            try {
                Files.move(sourcePct, targetPct, REPLACE_EXISTING);
            } catch (IOException e) {
                logger.log(Level.SEVERE, "IOException while dealing with pictures moved to the directory.\n" + e.getMessage());
            }

        } else {
            File folderOt = new File(StringPath + ("\\Other"));

            if (!folderOt.exists()) {
                folderOt.mkdir();
                logger.log(Level.INFO, "Created folder named \"Other\"");
            }

            Path sourceOt = path.resolve(FileName);
            Path targetOt = folderOt.toPath().resolve(FileName);

            try {
                Files.move(sourceOt, targetOt, REPLACE_EXISTING);
            } catch (IOException e) {
                logger.log(Level.SEVERE, "IOException while dealing with other files moved to the directory.\n" + e.getMessage());
            }

        }
        logger.exiting(getClass().getName(), "sortIt");
    }
}
